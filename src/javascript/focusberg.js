/**
 * @file
 * JavaScript file for the focusberg module.
 */

import '../sass/focusberg.scss';

(function ($, Drupal, drupalSettings, Drupalfocusberg) {
  const $body = $('body');
  const $focusbergTrigger = $('.toolbar-icon-focusberg')

  'use strict';

  /**
   * focusberg module namespace.
   *
   * @namespace
   *
   * @todo put this in Drupal.focusberg to expose it.
   */
  Drupalfocusberg = Drupalfocusberg || {};

  /**
   * * Attaches focusberg module behaviors.
   *
   * @type {Drupal~behavior}
   * @prop {Drupal~behaviorAttach} attach
   * * Attach focusberg functionality to the page.
   *
   */

  Drupal.behaviors.focusberg = {
    attach: function () {

      $body.once('focusberg').each(function () {

        $('.toolbar-icon-focusberg').click(function (event) {
          event.preventDefault();
          Drupalfocusberg.focusbergToggle();
        });

        // * Key events.
        $(document).keydown(function (event) {
          // * Toggle on alt/option + F.
          if (event.altKey === true &&
            (event.keyCode === 70)) {
            Drupalfocusberg.focusbergToggle();
            event.preventDefault();
          }
        });
      });
    }
  };

  /**
   * * Toggle focus mode
   */
  Drupalfocusberg.focusbergToggle = function () {
    $focusbergTrigger.toggleClass('toolbar-icon-focusberg--activated');
    $focusbergTrigger.parent().toggleClass('focusberg--activated');
    $focusbergTrigger.text(function (i, text) {
      return text === "Focused" ? "Focus" : "Focused";
    });
    if ($body.is('.focusberg--enabled')) {
      console.log('toggled OFF');
      $body.removeClass('focusberg--enabled').toggleClass('toolbar-fixed');
      $('.gutenberg__editor').attr('id', '');
    } else {
      console.log('toggled ON');
      $body.addClass('focusberg--enabled').toggleClass('toolbar-fixed');
      $('.gutenberg__editor').attr('id', 'focused');
    }
  };

})(jQuery, Drupal, drupalSettings);
